const express = require('express');
const app = express();
const port = 3000;
const mysql = require('mysql');
const bodyParser = require('body-parser')

app.set('view engine', 'pug');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'demo'
});

connection.connect(function (err) {
	if (err) throw err;
	console.log("Connected!");
});

//app.get('/',(req,res) => res.send('hello world'));
// app.get('/', (req, res) => {
// 	connection.query('SELECT * FROM posts', (err, result) => {
// 		res.render('index', {
// 			posts: result
// 		});
// 	})
// });

app.get('/add', (req, res) => {
	res.render('add');
});

app.post('/add', (req, res) => {
	const title = req.body.title;
	const content = req.body.content;
	const author_name = req.body.author_name;
	const post = {
		title: title,
		content: content,
		author: author_name,
		created_at: new Date()
	}
	connection.query('INSERT INTO posts SET ?', post, (err) => {
		console.log('Data Inserted');
		return res.redirect('/');
	});
});

app.get('/edit/:id', (req, res) => {

	const edit_postID = req.params.id;

	connection.query('SELECT * FROM posts WHERE id=?', [edit_postID], (err, results) => {
		if (results) {
			res.render('edit', {
				post: results[0]
			});
		}
	});
});

app.post('/edit/:id', (req, res) => {
	const update_title = req.body.title;
	const update_content = req.body.content;
	const update_author_name = req.body.author_name;
	const userId = req.params.id;
	connection.query('UPDATE `posts` SET title = ?, content = ?, author = ? WHERE id = ?', [update_title, update_content, update_author_name, userId], (err, results) => {
		if (results.changedRows === 1) {
			console.log('Post Updated');
		}
		return res.redirect('/');
	});
});

app.get('/delete/:id', (req, res) => {
	connection.query('DELETE FROM `posts` WHERE id = ?', [req.params.id], (err, results) => {
		return res.redirect('/');
	});
});

app.get('/', (req, res) => {
	res.render("Login", { status: "" })
});

app.post('/', (req, res) => {
	email = req.body.email
	password = req.body.password
	connection.query('SELECT * FROM `mst_person_information` WHERE email= ? and password= ?', [email, password], (err, resultPerson) => {
		if (err) throw err
		if (resultPerson) {
			if (resultPerson.length === 0) {
				res.render("Login", { status: "Username or password wrong" })
			} else {
				connection.query('SELECT * FROM posts', (err, resultPost) => {
					res.render('index', {
						posts: resultPost,
						person: resultPerson[0]
					});
				})
			}
		}
	});
});

//----------------Person--------------//
app.get('/person', (req, res) => {
	connection.query('SELECT * FROM mst_person_information', (err, resultPerson) => {
		res.render('person/index', {
			person: resultPerson
		});
	})
});

app.get('/person/edit/:id', (req, res) => {

	const edit_personID = req.params.id;

	connection.query('SELECT * FROM mst_person_information WHERE id=?', [edit_personID], (err, results) => {
		if (results) {
			res.render('person/edit', {
				person: results[0]
			});
		}
	});
});

app.post('/person/edit/:id', (req, res) => {
	const update_name = req.body.name;
	const update_age = req.body.age;
	const update_email = req.body.email;
	const update_password = req.body.password;
	const update_status = req.body.status;
	const userId = req.params.id;
	connection.query('UPDATE `mst_person_information` SET name = ?, age = ?, email = ? , password = ?, status = ?  WHERE id = ?', [update_name, update_age, update_email,update_password,update_status, userId], (err, results) => {
		if (results.changedRows === 1) {
			console.log('Person Updated');
		}
		return res.redirect('/person');
	});
});

app.get('/person/add', (req, res) => {
	res.render('person/add');
});

app.post('/person/add', (req, res) => {
	const name = req.body.name;
	const age = req.body.age;
	const email = req.body.email;
	const password = req.body.password;
	const status = req.body.status;
	const post = {
		name: name,
		age: age,
		email: email,
		password: password,
		status: status
	}
	connection.query('INSERT INTO `mst_person_information` SET ?', post, (err) => {
		if(err) throw err
		console.log('Data Inserted');
		return res.redirect('/person');
	});
});

app.get('/person/delete/:id', (req, res) => {
	connection.query('DELETE FROM `mst_person_information` WHERE id = ?', [req.params.id], (err, results) => {
		return res.redirect('/person');
	});
});

app.listen(port, () => console.log(`listening on port ${port}!`))